/*
* lib.hpp
* Created on: Jun 18, 2023
* Author: Sebastian Szczepański
*/

#ifndef LIB_HPP_
#define LIB_HPP_
#include <map>

std::map<std::string, std::string> getDataFromFile(std::string fileName);

#endif