#include <iostream>
#include <fstream>
#include <sstream>
//#include <map>

#include "lib.hpp"
using namespace std;

std::map<string, string> getDataFromFile(string fileName)
{
	ifstream getData;
	getData.open(fileName);
	if (getData.is_open())
	{
		std::map<string, string> data;
		getData.seekg(0, ios::end);
		int file_size = getData.tellg();
		string line;
		while (getline(getData, line))
		{
			data[line] = line;
		}
		/*while (std::getline(infile, line))
		{
			std::istringstream iss(line);
			int a, b;
			if (!(iss >> a >> b)) { break; } // error

			// process pair (a,b)
		 }*/
		getData.close();
		
		return data;
	}
}