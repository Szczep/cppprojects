#include <iostream>
#include <vector>
#include "lib.hpp"
using namespace std;

int main()
{
	//vector<string> data = getDataFromFile("data.csv")
	std::map<string, string> data = getDataFromFile("data.csv");
	for (auto it : data)
	{
		cout << it.first << endl;
	}
	return 0;
}